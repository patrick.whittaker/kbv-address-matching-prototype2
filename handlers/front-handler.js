const url = require("url");
const { table } = require('../data/road-name-conversion-table');

const checkPostCodes = ((uPostCode, cPostCode) => {
  let userPostCode, cisPostCode;

  if (uPostCode) {
    userPostCode = processPunctuation(uPostCode);
    userPostCode = userPostCode.replace(/\s/g, '');
    userPostCode = userPostCode.toUpperCase();
  };

  if (cPostCode) {
    cisPostCode = processPunctuation(cPostCode);
    cisPostCode = cisPostCode.replace(/\s/g, '');
    cisPostCode = cisPostCode.toUpperCase();
  };


  if (userPostCode !== '' && cisPostCode !== '') {
    if (userPostCode === cisPostCode) {
      postCodeMatch = true;
    } else {
      postCodeMatch = false;
    };
  } else {
    postCodeMatch = true;
  };

  return [postCodeMatch, userPostCode, cisPostCode];
})

const processPunctuation = ((addressLine) => {
  //Remove all punctuation and extraneous spaces
  let newAddress = addressLine.replace(/[.,'"\/#!$%\^&\*;:{}=\-_`~()]/g, "");
  return newAddress.trim();
});

const processAddress = (address) => {
  // Step 2: Remove all punctuation and extraneous spaces
  let newAddress = processPunctuation(address);

  // Step 3: Change to upper case
  newAddress = newAddress.toUpperCase();
  const addressElements = newAddress.split(' ');

  // Step 4: Substitute abbreviations for full words 

  for (let i = 0; i < addressElements.length; i++) {
    table.forEach((abbrev) => {
      if (addressElements[i] === abbrev[0]) {
        addressElements[i] = abbrev[1];
      }
    })
  };

  // Step 5: Sort address elements
  addressElements.sort();
  newAddress = '';
  for (let i = 0; i < addressElements.length; i++) {
    newAddress += addressElements[i] + ' ';
  };

  newAddress = newAddress.substring(0, newAddress.length - 1);
  newAddress = newAddress.trim();
  return newAddress;
};

const frontHandler = async (req, res) => {
  const u = url.parse(req.originalUrl, true);
  let ul1 = u.query.uiLine1;
  let ul2 = u.query.uiLine2;
  let cl1 = u.query.cisLine1;
  let cl2 = u.query.cisLine2;
  let uTown = u.query.uTown;
  let uPostCode = u.query.uPostCode;
  let cTown = u.query.cisTown;
  let cPostCode = u.query.cisPostCode;

  let userAddress1 = '';
  let cisAddress1 = '';
  let userTown = '';
  let cisTown = '';
  let userPostCode = '';
  let cisPostCode = '';
  let postCodeMatch = true;

  let matched = 'NOT MATCHED';

  if (ul1 && cl1) {
    // Step 1: Concatenate lines 1 & 2
    userAddress1 = `${ul1} ${ul2}`;
    cisAddress1 = `${cl1} ${cl2}`;

    userAddress1 = processAddress(userAddress1);
    cisAddress1 = processAddress(cisAddress1);
    userTown = processPunctuation(uTown);
    cisTown = processPunctuation(cTown).replace(/\s/g, '');
    userTown = userTown.toUpperCase().replace(/\s/g, '');
    ;
    cisTown = cisTown.toUpperCase();

    if (uPostCode && cPostCode) {
      const pc = checkPostCodes(uPostCode, cPostCode);
      postCodeMatch = pc[0];
      userPostCode = pc[1];
      cisPostCode = pc[2];
    };

    if (userAddress1 === cisAddress1 && userTown === cisTown && postCodeMatch) {
      matched = 'MATCHED';
    };
  };

  res.render("front.njk", {
    ul1,
    ul2,
    cl1,
    cl2,
    userAddress1,
    cisAddress1,
    uTown,
    cTown,
    uPostCode,
    cPostCode,
    userTown,
    cisTown,
    userPostCode,
    cisPostCode,
    matched,
  });
};

module.exports = { frontHandler };

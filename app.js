const express = require('express');
const app = express();

const frontRouter = require('./routes/front.js');

const nunjucks = require('nunjucks');
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
const PORT = 3000;

nunjucks.configure('pages', {
    autoescape: true,
    express: app
});

app.use(express.static(__dirname + '/'));

app.use('/', frontRouter);
app.use('/front', frontRouter);

app.listen(PORT, (err) => {
    console.log(`Addy is up & running on port ${PORT}`);
});
